// SERVER-SIDE CODE

// indicate strict enforcement of JS syntax
"use strict";

console.log("Starting...")
var express = require("express");
var app = express();
var bodyParser = require("body-parser");

app.use(express.static(__dirname + "/../client/"));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const APP_PORT = process.env.NODE_PORT || 3000;

var questionSet = [{
        questionId: 0,
        question:"Javascript is _________ language.",
        answers: [{label:"Programming", answerValue:1},
                  {label:"Application", answerValue:2},
                  {label:"Scripting", answerValue:3},
                  {label:"None of the above", answerValue:4}],
        correctAnswer:3
    },
    {
        questionId:1,
        question:"JavaScript is ______ Side Scripting Language.",
        answers: [{label:"Server", answerValue:1},
                {label:"ISP", answerValue:2},
                {label:"Browser", answerValue:3},
                {label:"None of the above", answerValue:4}],
        correctAnswer:3
    },
    {
        questionId:2,
        question:"JavaScript is designed for following purpose:",
        answers: [{label:"To Style HTML Pages", answerValue:1},
                  {label:"To add interactivity to HTML Pages", answerValue:2},
                  {label:"To Perform Server Side Scripting Opertion", answerValue:3},
                  {label:"To Execute Query Related to DB on Server", answerValue:4}],
        correctAnswer:2
    },
    {
        questionId:3,
        question:"JavaScript is a/an ________ language.",
        answers: [{label:"Compiled", answerValue:1},
                  {label:"Natural", answerValue:2},
                  {label:"English", answerValue:3},
                  {label:"Interpreted", answerValue:4}],
        correctAnswer:2
    },
    {
        questionId:4,
        question:"JavaScript Code is written inside file having extension __________.",
        answers: [{label:".js", answerValue:1},
                  {label:".jvs", answerValue:2},
                  {label:".javascript", answerValue:3},
                  {label:".jsc", answerValue:4}],
        correctAnswer:1
    }
];

// serves back request for a new question set
app.get("/mcquestions", function(req, res) {
    // randomise question to pick
    var x = Math.random() * (6 - 1) + 1;
    var y = Math.floor(x);
    console.log(y);
    // return the randomly-selected question set
    res.json(questionSet[y-1]);
});

// accepts submitted answer, checks if correct & returns status
app.post("/submit", function(req,res) {
    console.log("Server received: " + JSON.stringify(req.body));

    var submittedAnswer = req.body;
    var expectedAnswer = questionSet[submittedAnswer.questionId];
    
    if (parseInt(submittedAnswer.answerValue) == expectedAnswer.correctAnswer)
        submittedAnswer.isCorrect = true;
    else
        submittedAnswer.isCorrect = false;
    res.status(200).json(submittedAnswer);
});

// Alternative 404 error message
app.use(function(req, res, next) {
    res.send("<h2>Sorry, no such page.</h2>");
    next();
});

// start the actual web service
app.listen(APP_PORT, function() {
    console.log("MCQ web app started at " + APP_PORT);
});