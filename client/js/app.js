// CLIENT-SIDE CODE

(function(){
    // indicate strict enforcement of JS syntax
    "use strict";

    var app = angular.module("mcqApp", []);
    app.controller("mcqController", ["$http", mcqController]);

    // describe controller abilities
    function mcqController($http) {
        var self = this;

        // to hold the question and answer options from server
        self.questionSet = {
        };

        // to hold the user responses to send to the server
        self.finalAnswer = {
            questionId: 0,
            answerValue: "",
            serverMessage: ""
        };

        // to get a new set of question & answer options from the server
        self.initForm = function(){
            $http.get("/mcquestions")
            .then(function(result) {
                console.log(result);
                self.questionSet = result.data;
            }).catch(function(e) {
                console.log(e);
            });
        }

        self.initForm();

        self.validateAnswer = function() {
            console.log("Submitting answer for validation...");
            self.finalAnswer.questionId = self.questionSet.questionId;
            $http.post("/submit", self.finalAnswer)
                .then (function(result) {
                    console.log(result);
                    if (result.data.isCorrect) {
                        self.finalAnswer.serverMessage = "CORRECT";
                    } else {
                        self.finalAnswer.serverMessage = "INCORRECT. Try Again";
                    }
                }) .catch(function(e) {
                    console.log(e);
                });
        }
    }
}) ();

console.log("started client app.js");